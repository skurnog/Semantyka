import gzip
import io
import os
import xml.etree.ElementTree as ET

main_corpus_dir = r'd:\Downloads\nkjp-wikipedia\\'
parsed_file = io.open(r'Z:\projekty\PycharmProjects\Semantyka\resources\nkjp_wiki_parsed.txt', 'w', encoding='utf-8')


def parse_file(f):
    basic_forms = []
    segments = ET.parse(f).getroot()[1][1][0][0]
    for segment_s in segments:
        for seg in segment_s:
            morph_seg = seg[0]
            basic_form = morph_seg[1 if len(morph_seg) == 3 else 2][0][0][0].text
            if basic_form is not None and len(basic_form) > 1 and not basic_form.isspace():
                basic_forms.append(basic_form)
    return basic_forms


for root, dirs, files in os.walk(main_corpus_dir):
    i = 1
    files_num = str(len(dirs))
    for name in dirs:
        file_name = os.path.join(root, name, 'ann_morphosyntax.xml.gz')
        print(str(i) + "/" + files_num + " Decompressing " + file_name)
        xml_file = gzip.open(file_name)
        print(str(i) + "/" + files_num + " Parsing " + file_name)
        parsed_data = parse_file(xml_file)
        print(str(i) + "/" + files_num + " Writing " + file_name)
        parsed_file.write(' '.join(parsed_data))
        parsed_file.write('\n')
        parsed_file.flush()
        xml_file.close()
        i += 1
print("DONE")
