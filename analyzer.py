import io
import pprint
import csv

WORD_DISTANCE = 12
WINDOW_SIZE = 1
ALPHA = 0.66
BETA = 0.00002
GAMMA = 0.00002


def count_phrases(corpus_parsed):
    corpus_last_index = len(corpus_parsed) - 1
    word_occurrences = {}

    for word_index in range(corpus_last_index + 1):
        word = corpus_parsed[word_index]
        word_occurrences[word] = word_occurrences.get(word, 0) + 1

        # count phrases in corpus
        if word_index != corpus_last_index:
            local_windows_size = WINDOW_SIZE if word_index + WINDOW_SIZE <= corpus_last_index \
                else corpus_last_index - word_index
            window_upper_boundaries = range(1, local_windows_size + 1)
            for window_internal_index in window_upper_boundaries:
                phrase = ' '.join(
                    phrase_part for phrase_part in corpus_parsed[word_index: word_index + window_internal_index])
                word_occurrences[phrase] = word_occurrences.get(phrase, 0) + 1
    return word_occurrences


def find_pairs(stimulus_word, corpus_parsed):
    print('Searching for pairs of \'%s\' in the distance of %d words.' % (stimulus_word, WORD_DISTANCE))
    corpus_last_index = len(corpus_parsed) - 1

    stimulus_word_pair_dict = {}
    for word_index in range(len(corpus_parsed) - 1):
        if len(stimulus_word.split(' ')) == 1:
            current_phrase = corpus_parsed[word_index]
        else:
            current_phrase = ' '.join([corpus_parsed[word_index], corpus_parsed[word_index + 1]])

        if current_phrase == stimulus_word:
            lower_bound = -WORD_DISTANCE if word_index >= WORD_DISTANCE else -word_index
            upper_bound = WORD_DISTANCE + 1 if len(corpus_parsed) - word_index > WORD_DISTANCE else len(corpus_parsed) - word_index
            for i in range(lower_bound, upper_bound):
                if i == 1:
                    continue
                local_windows_size = WINDOW_SIZE if word_index + i + WINDOW_SIZE <= corpus_last_index else corpus_last_index - word_index
                for j in range(local_windows_size):
                    coocured_phrase = ' '.join(phrase_part for phrase_part in corpus_parsed[word_index + i: word_index + i + j + 1])
                    if coocured_phrase and coocured_phrase != stimulus_word and stimulus_word not in coocured_phrase:
                        stimulus_word_pair_dict[coocured_phrase] = stimulus_word_pair_dict.get(coocured_phrase, 0) + 1
    return stimulus_word_pair_dict


def read_corpus_file(corpus_file_name):
    corpus_lines = io.open(corpus_file_name, encoding='utf-8').read().splitlines()
    corpus_parsed = []
    for line in corpus_lines:
        corpus_parsed += line.split(' ')
    return corpus_parsed


def process_corpus_file(corpus_file_name, stimulus_list):
    cooccured_pairs = {}

    corpus_parsed = read_corpus_file(corpus_file_name)
    word_occurrences = count_phrases(corpus_parsed)
    for stimulus_word in stimulus_list:
        cooccured_pairs[stimulus_word] = find_pairs(stimulus_word, corpus_parsed)

    return cooccured_pairs, word_occurrences


def calculate_predictions(coocured_pairs, word_occurrences):
    corpus_length = sum(word_occurrences.values())
    r_dict = {}

    for stimulus_word in coocured_pairs.keys():
        stimulus_dict = {}
        for pair_word, pair_occurrences in coocured_pairs[stimulus_word].items():
            if word_occurrences[pair_word] > BETA * corpus_length:
                stimulus_dict[pair_word] = pair_occurrences/pow(word_occurrences[pair_word], ALPHA)
            else:
                stimulus_dict[pair_word] = pair_occurrences/(GAMMA * corpus_length)
        r_dict[stimulus_word] = sorted(stimulus_dict.items(), key=lambda x: x[1], reverse=True)
    return r_dict


def generate_logs(r_vec):
    def remove_polish_chars(string):
        polish_chars = {'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ż': 'z', 'ź': 'z'}
        res_str = []
        for ch in string:
            res_str.append(polish_chars.get(ch, ch))
        return ''.join(res_str)

    res_fname_template = 'logs/raport_%s_%d_%d_%f_%f_%f.csv' % ('%s', WORD_DISTANCE, WINDOW_SIZE, ALPHA, BETA, GAMMA)
    for stim in r_vec.keys():
        res_f = io.open(res_fname_template % remove_polish_chars(stim), 'w', encoding='utf-8')
        for coocur in r_vec[stim]:
            res_f.write('%s,%s\n' % (coocur[0], coocur[1]))


occp, occl = process_corpus_file('resources/nkjp_parsed.txt', ['duży', 'na świat', 'okno', 'szyba'])
vec = calculate_predictions(occp, occl)

generate_logs(vec)
