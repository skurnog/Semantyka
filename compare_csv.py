import io
from pprint import pprint
from os.path import join as path_join
from os import listdir
import os

REL_PATH = 'skojarzenia'
DST_REL_PATH = 'skojarzenia_clean'


def reformat_csv():
    for file in listdir(REL_PATH):
        csv_file = io.open(path_join(REL_PATH, file), 'r', encoding='utf-8')
        clean_csv_file = io.open(path_join(DST_REL_PATH, file), 'w+', encoding='utf-8')

        res_lines = []
        for line in csv_file.readlines():
            line = line.strip().split(',')
            res_lines.append(','.join([line[1], line[0]]))
        clean_csv_file.write('\n'.join(res_lines))


def compare_csv(csv_filepath1, csv_filepath2):
    csv_file1_lines = io.open(csv_filepath1, 'r', encoding='utf-8').readlines()
    csv_file2_lines = io.open(csv_filepath2, 'r', encoding='utf-8').readlines()
    # filename1 = os.path.split(csv_filepath1)[-1].split('.')[0]
    filename2 = os.path.split(csv_filepath2)[-1]
    csv_diff_file = io.open('diff/diff_%s' % (filename2), 'w+', encoding='utf-8')

    process_csv = \
        lambda csv_file_lines: {tokens[0]: tokens[1] for tokens in
                                [token for token in [line.strip().split(',') for line in csv_file_lines]]}

    csv_file1_dict = process_csv(csv_file1_lines)
    csv_file2_dict = process_csv(csv_file2_lines)
    common_keys = list(set(csv_file1_dict.keys()).intersection(csv_file2_dict.keys()))

    result_csv_lines = []
    for common_key in common_keys:
        values = csv_file1_dict[common_key], csv_file2_dict[common_key]
        result_csv_lines.append(','.join([common_key, values[0], values[1]]))

    pprint(result_csv_lines)
    csv_diff_file.write('\n'.join(result_csv_lines))


for f1 in listdir('skojarzenia_clean'):
    for f2 in listdir('logs'):
        print(f1)
        if f1.split('.')[0] in f2.split('_')[1]:
            print('ss')
            compare_csv(path_join('skojarzenia_clean', f1), path_join('logs', f2))
