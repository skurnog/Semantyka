import os
import io
import xml.etree.ElementTree as ET

main_corpus_dir = r'd:\Downloads\NKJP-PodkorpusMilionowy-1.1'
parsed_file = io.open(r'Z:\projekty\PycharmProjects\Semantyka\resources\parsed.txt', 'w', encoding='utf-8')


def parse_file(f):
    basic_forms = []
    segments = ET.parse(f).getroot()[1][1][0][0]
    for segment_s in segments:
        for seg in segment_s:
            morph_seg = seg[0]
            basic_form = morph_seg[1 if len(morph_seg) == 3 else 2][0][0][0].text
            if basic_form is not None and len(basic_form) > 1:
                basic_forms.append(basic_form)
    return basic_forms


for root, dirs, files in os.walk(main_corpus_dir):
    i = 0
    for name in dirs:
        file_name = os.path.join(root, name, 'ann_morphosyntax.xml')
        ann_morphosyntax = io.open(file_name, encoding='utf-8')
        basic_forms_parsed = parse_file(ann_morphosyntax)

        parsed_file.write(' '.join(basic_forms_parsed))
        parsed_file.write('\n')

        i += 1
        print("#" + str(i))
print("DONE")
